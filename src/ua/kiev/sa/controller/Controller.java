package ua.kiev.sa.controller;

public interface Controller {

    void processInput();

}
