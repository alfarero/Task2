package ua.kiev.sa.controller;

import ua.kiev.sa.model.Model;
import ua.kiev.sa.util.*;
import ua.kiev.sa.view.View;


public class ControllerImpl implements Controller {

    private Model model;
    private View view;
    private ValidationHelper validationHelper;
    private InputHelper inputHelper;

    public ControllerImpl(Model model, View view) {
        this.model = model;
        this.view = view;
        inputHelper = new InputHelperImpl();
        validationHelper = new ValidationHelperImpl();
    }

    @Override
    public void processInput() {
        view.print(Constants.GREETINGS);
        view.print(String.format(Constants.PROMPT, Constants.MIN_KEY_VALUE, Constants.MAX_KEY_VALUE));
        enteringNumber();
    }

    private void enteringNumber() {
        while (true) {
            int inputNumber = getInputNumber();
            analyzeInputNumber(inputNumber);
        }
    }

    private void analyzeInputNumber(int inputNumber) {
        if (validationHelper.validateInputIsInRange(inputNumber, model.getMinValue(), model.getMaxValue())) {
            int diff = inputNumber - model.getKey();
            if (diff == 0) {
                processCorrectInputNumber(inputNumber);
            } else {
                processIncorrectInputNumber(inputNumber, diff);
            }
        } else {
            view.print(Constants.NOT_IN_RANGE_MESSAGE);
            view.print(String.format(Constants.PROMPT_REPEAT_INPUT, model.getMinValue(), model.getMaxValue()));
        }
    }

    private void processIncorrectInputNumber(int inputNumber, int diff) {
        view.print(Constants.ENTERED_NUMBER_MESSAGE + inputNumber);
        if (diff > 0) {
            view.print(String.format(Constants.COMPARISION_MESSAGE, "bigger"));
            model.setMaxValue(inputNumber);
        } else {
            view.print(String.format(Constants.COMPARISION_MESSAGE, "less"));
            model.setMinValue(inputNumber);
        }
        model.getPreviousAttempts().add(inputNumber);
        view.print(String.format(Constants.PROMPT_REPEAT_INPUT, model.getMinValue(), model.getMaxValue()));
    }

    private void processCorrectInputNumber(int inputNumber) {
        boolean rightKey;
        model.getPreviousAttempts().add(inputNumber);
        view.print(String.format(Constants.ENTERED_NUMBER_MESSAGE, inputNumber));
        view.print(String.format(Constants.CONGRATULATION, model.getKey()));
        view.print(String.format(Constants.NUMBER_OF_ATTEMPTS, model.getPreviousAttemptsCount()));
        view.print(Constants.STATISTICS_MESSAGE + model.getPreviousAttempts().toString());
        System.exit(0);
    }

    private int getInputNumber() {
        String inputText = inputHelper.getInput();
        int inputNumber = 0;
        try {
            inputNumber = validationHelper.validateInputIsNumber(inputText);
        } catch (NumberFormatException exception) {
            view.print(Constants.WRONG_SYMBOL);
        }
        return inputNumber;
    }
}
