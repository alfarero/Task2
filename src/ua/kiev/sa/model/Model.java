package ua.kiev.sa.model;


import java.util.List;

public interface Model {

    int getKey();

    void setMinValue(int minValue);

    int getMinValue();

    int getMaxValue();

    void setMaxValue(int maxValue);


    int getPreviousAttemptsCount();

    List<Integer> getPreviousAttempts();
}
