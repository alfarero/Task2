package ua.kiev.sa.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ModelImpl implements Model {

    private int key;
    private int minValue;
    private int maxValue;
    private List<Integer> previousAttempts = new LinkedList<>();


    public ModelImpl(int minValue, int maxValue) {
        Random randomGenerator = new Random();
        key = randomGenerator.nextInt(maxValue-minValue)+minValue;
        this.minValue=minValue;
        this.maxValue=maxValue;
    }

    @Override
    public int getKey() {
        return key;
    }

    @Override
    public int getPreviousAttemptsCount() {
        return previousAttempts.size();
    }

    @Override
    public List<Integer> getPreviousAttempts() {
        return previousAttempts;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
}
