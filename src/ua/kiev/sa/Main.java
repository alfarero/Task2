package ua.kiev.sa;

import ua.kiev.sa.controller.Controller;
import ua.kiev.sa.controller.ControllerImpl;
import ua.kiev.sa.model.Model;
import ua.kiev.sa.model.ModelImpl;
import ua.kiev.sa.util.Constants;
import ua.kiev.sa.view.View;
import ua.kiev.sa.view.ViewImpl;

/**
 * Created by Admin on 7/9/2018.
 */
public class Main {

    public static void main(String[] args) {
        Model model = new ModelImpl(Constants.MIN_KEY_VALUE, Constants.MAX_KEY_VALUE);
        View view = new ViewImpl();
        Controller controller = new ControllerImpl(model, view);
        controller.processInput();

    }
}
