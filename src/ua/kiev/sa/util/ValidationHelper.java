package ua.kiev.sa.util;

/**
 * Created by Admin on 7/9/2018.
 */
public interface ValidationHelper {

    int validateInputIsNumber(String input);
    boolean validateInputIsInRange(int input, int minValue, int maxValue);


}

