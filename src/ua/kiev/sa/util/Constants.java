package ua.kiev.sa.util;

/**
 * Created by Admin on 7/10/2018.
 */
public class Constants {
    public static final int MIN_KEY_VALUE = 0;
    public static final int MAX_KEY_VALUE = 100;

    public static final String GREETINGS = "Hello! Try to find out the key!";
    public static final String PROMPT = "Enter the key between %d and %d";

    public static final String WRONG_SYMBOL = "You've entered incorrect symbol";
    public static final String ENTERED_NUMBER_MESSAGE = "You've entered number ";

    public static final String CONGRATULATION = "Congratulations! You've found out the number ";
    public static final String NUMBER_OF_ATTEMPTS = "You've made %d attempts";
    public static final String STATISTICS_MESSAGE = "and entered next numbers: ";
    public static final String COMPARISION_MESSAGE = "Your number is %s than key";
    public static final String PROMPT_REPEAT_INPUT = "Try once more! Enter the number between %d and %d";
    public static final String NOT_IN_RANGE_MESSAGE = "Entered number is not in range";
}
