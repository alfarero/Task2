package ua.kiev.sa.util;

/**
 * Created by Admin on 7/9/2018.
 */
public class ValidationHelperImpl implements ValidationHelper {

    @Override
    public int validateInputIsNumber(String input) {
        return Integer.parseInt(input);
    }

    @Override
    public boolean validateInputIsInRange(int input, int minValue, int maxValue) {
        return input>minValue&&input<maxValue;
    }


}
