package ua.kiev.sa.util;

import java.util.Scanner;

/**
 * Created by Admin on 7/9/2018.
 */
public class InputHelperImpl implements InputHelper {

    private Scanner scanner;

    public InputHelperImpl(){
        scanner=new Scanner(System.in);
    }

    public String getInput() {
        return scanner.next();
    }
}
